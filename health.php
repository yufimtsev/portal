<?php
$servername = getenv(strtoupper(getenv("OPENSHIFT_MYSQL_SERVICE_NAME"))."_SERVICE_HOST");
$username = getenv("OPENSHIFT_MYSQL_DB_USERNAME");
$password = getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
// Create connection
$conn = new mysqli($servername, $username, $password);
// Check connection
if ($conn->connect_error) {
    header("HTTP/1.1 503 Service Unavailable");
    die("Connection failed: " . $conn->connect_error);
}
echo "OK";
?>